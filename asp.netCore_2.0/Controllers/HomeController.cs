﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asp.netCore_2.Interfaces;
using asp.netCore_2.Interfaces.Implementation;
using asp.netCore_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace asp.netCore_2._0.Controllers
{
    public class HomeController : Controller
    {
        private IStudentRepository<Student> _studentRepository;
        public HomeController(IStudentRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public IActionResult Index()
        {
            var students = _studentRepository.GetAll();
            return View(students);
        }

        public IActionResult Details(int id)
        {
            var student = _studentRepository.GetOne(id);
            return View(student);
        }

        public IActionResult Delete(int id) 
        {
            var student = _studentRepository.GetOne(id);
            return View(student);
        }

        [HttpPost]
        public IActionResult Delete(Student student)
        {
            _studentRepository.Delete(student.Id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var student =_studentRepository.GetOne(id);
            return View(student);
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            _studentRepository.Edit(student.Id, student);
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
       
            var students = _studentRepository.GetAll();
            var maxId = students.Max(x => x.Id);
            student.Id = ++maxId;
            var ifReady =  _studentRepository.Create(student);
  
            return RedirectToAction("Index");
        }
    }
}
