﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.netCore_2.Interfaces
{
    public interface IStudentRepository<T> where T : class
    {
        bool Create(T item);
        T GetOne(int id);
        void Edit(int id, T item);
        List<T> GetAll();
        void Delete(int id);
    }
}
