﻿using asp.netCore_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.netCore_2.Interfaces.Implementation
{
    public class MockRepository : IStudentRepository<Student>
    {
        List<Student> students;
        public MockRepository()
        {
            students = new List<Student>()
            {
            new Student{ Id = 1, Name = "Bruce Wayne"},
            new Student{ Id = 2, Name = "Barry Allen"},
            new Student{ Id = 3, Name = "John Johnzz"},
            new Student{ Id = 4, Name = "Clark Kent"},
            new Student{ Id = 5, Name = "Lex Luthor"},
            };
        }

        //public void Create(T item)
        //{
        //    context.Add(item);
        //}

        //public void Edit(T item)
        //{
        //    context.FirstOrDefault(x => x.Equals(item));
        //}

        //public T GetOne(T item)
        //{
        //    return context.FirstOrDefault(x => x.Equals(item));
        //}
        public bool Create(Student item)
        {
            try
            {
                students.Add(item);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public void Delete(int id)
        {
            var student = students.Where(x => x.Id == id).FirstOrDefault();
            students.Remove(student);
        }



        public void Edit(int id, Student item)
        {
            var studentIndex = students.FindIndex(i => i.Id == id);
            students[studentIndex] = item;
        }

        public List<Student> GetAll()
        {
            return students;
        }

        public Student GetOne(int id)
        {
            var student = students.Where(x => x.Id == id).FirstOrDefault();
            return student;
        }

    }
}
